 --- OK.
 Match tree built with 23 nodes.

PDDL problem description loaded: 
	Domain: MONKEY_BOX_BANANAS
	Problem: MONKEY_BOX_BANANAS
	#Actions: 23
	#Fluents: 17
Landmarks found: 3
Starting search with IW (time budget is 60 secs)...
rel_plan size: 5
#RP_fluents 5
Caption
{#goals, #UNnachieved,  #Achieved} -> IW(max_w)

{3/3/0}:IW(1) -> [2][3][4][5][6][7][8][9]rel_plan size: 3
#RP_fluents 4
{3/2/1}:IW(1) -> [2][3]rel_plan size: 1
#RP_fluents 1
{3/1/2}:IW(1) -> [2]rel_plan size: 0
#RP_fluents 0Plan found with cost: 11
Total time: 4.47035e-10
Nodes generated during search: 41
Nodes expanded during search: 30
IW search completed