(define (problem monkey_box_bananas) (:domain monkey_box_bananas)

(:objects 
    bn                  - bananas
    mon                 - monkey
    push_bx unpush_bx   - box       ; two boxes, one is pushable another not
    loc_a loc_b loc_c   - loc
)

(:init
    (near loc_a loc_b) (near loc_b loc_a)
    (near loc_b loc_c) (near loc_c loc_b)

    (at mon loc_c) (with_empty_hands mon)

    (hanging bn loc_b)

    (at push_bx loc_a) (pushable push_bx)

    (at unpush_bx loc_c) 
)

(:goal (and        ; monkey has bananas, monkey and box are at initial locations
    (has mon bn)
    (at push_bx loc_a) 
    (at unpush_bx loc_c)
    (at mon loc_c)
))

)
