(define (domain monkey_box_bananas)

(:requirements 
    :strips 
    :typing 
    :equality 
    :adl
)

(:types
    loc locatable - object
    monkey box bananas - locatable
)

(:predicates
    (at ?obj - locatable ?loc - loc)
    (near ?l1 ?l2 - loc)
    (on ?a - monkey ?b - box)
    (hanging ?b - bananas ?l - loc)
    (has ?a - monkey ?b - bananas)
    (with_empty_hands ?a - monkey) 
   
    (pushable ?b - box) ; for D task
   
)

(:action go
    :parameters (?a - monkey
                 ?from ?to - loc)
    :precondition (and (at ?a ?from)
                       (near ?from ?to))
    :effect (and (not (at ?a ?from))
                 (at ?a ?to))
)

(:action Push
    :parameters (?a - monkey
                 ?b - box
                 ?from ?to - loc)
    :precondition (and (at ?a ?from)
                       (at ?b ?from)
                       (pushable ?b)
                       (with_empty_hands ?a)
                       (near ?from ?to)
                    )
    :effect (and (not (at ?a ?from))
                 (not (at ?b ?from))
                 (at ?a ?to)
                 (at ?b ?to))
)

(:action ClimbUp
    :parameters (?a - monkey
                 ?b - box
                 ?l - loc)
    :precondition (and (at ?a ?l)
                       (at ?b ?l)
                       (not (on ?a ?b)))
    :effect (and (on ?a ?b)
                 (not (at ?a ?l)))
)

(:action ClimbDown
    :parameters (?a - monkey
                 ?b - box
                 ?l - loc)
    :precondition (and (at ?b ?l)
                       (on ?a ?b))
    :effect (and (not (on ?a ?b))
                 (at ?a ?l))
)

(:action Grasp
    :parameters (?a - monkey
                 ?b - bananas
                 ?bx -box
                 ?l - loc)
    :precondition (and (at ?bx ?l)
                       (on ?a ?bx)
                       (hanging ?b ?l)
                       (not (has ?a ?b)))
    :effect (and (has ?a ?b)
                 (not (hanging ?b ?l))
                 (not (with_empty_hands ?a)))
)

(:action grasp_not_on_box
    :parameters (?a - monkey
                 ?b - bananas
                 ?l - loc)
    :precondition (and (at ?a ?l)            ; banana is at ground, not on a box
                       (at ?b ?l)            ; monkey is without it, on the ground
                       (not (has ?a ?b)))
    :effect (and (has ?a ?b)
                 (not (at ?b ?l))
                 (not (with_empty_hands ?a)))
)

(:action UnGrasp
    :parameters (?a - monkey
                 ?b - bananas
                 ?l - loc)
    :precondition (and (at ?a ?l)
                       (has ?a ?b))
    :effect (and (at ?b ?l)
                 (not (has ?a ?b))
                 (with_empty_hands ?a))
)
)