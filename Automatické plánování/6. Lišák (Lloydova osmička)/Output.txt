--- OK.
 Match tree built with 192 nodes.

PDDL problem description loaded: 
	Domain: LISAK
	Problem: LISAK
	#Actions: 192
	#Fluents: 81
Landmarks found: 9
Starting search with IW (time budget is 60 secs)...
rel_plan size: 18
#RP_fluents 27
Caption
{#goals, #UNnachieved,  #Achieved} -> IW(max_w)

{9/9/0}:IW(1) -> rel_plan size: 18
#RP_fluents 27
{9/8/1}:IW(1) -> [2]rel_plan size: 17
#RP_fluents 25
{9/7/2}:IW(1) -> [2][3][4][5][6][7][8][9][10][11][12][13][14][15][16][17][18][19][20][21][22][23][24][25][26][27][28][29][30][31][32][33][34][35][36];; NOT I-REACHABLE ;;
Total time: -8.64267e-10
Nodes generated during search: 532
Nodes expanded during search: 528
IW search completed
Starting search with BFS(novel,land,h_add)...
--[4294967295 / 29]--
--[6 / 29]--
--[5 / 29]--
--[5 / 26]--
--[5 / 24]--
--[5 / 21]--
--[5 / 20]--
--[5 / 18]--
--[5 / 12]--
--[4 / 12]--
--[4 / 10]--
--[3 / 10]--
--[3 / 8]--
--[3 / 5]--
--[3 / 2]--
--[2 / 2]--
--[2 / 0]--
--[0 / 0]--
Total time: 0.028
Nodes generated during search: 1275
Nodes expanded during search: 441
Plan found with cost: 58
BFS search completed