(define (domain lisak)

(:requirements 
    :strips 
    :typing 
    :equality 
    :adl
)

(:types
    position
    stone
)

(:predicates
    (at ?s - stone ?p - position)

    (empty ?p - position)
    (connected ?l1 ?l2 - position)
)

(:action move
    :parameters (?s - stone
                 ?from ?to - position)
    :precondition (and (connected ?from ?to)
                       (empty ?to)
                       (at ?s ?from))
    :effect (and (not (at ?s ?from))
                 (at ?s ?to)
                 (empty ?from)
                 (not (empty ?to)))
)

)