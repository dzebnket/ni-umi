(define (problem lisak) 
(:domain lisak)
(:objects 
    s1 s2 s3 s4 s5 s6 s7 s8 - stone
    p11 p12 p13 p21 p22 p23 p31 p32 p33 - position
)

(:init
    ;previous row                     same row                       next row
                        (connected p11 p12) (connected p11 p21)
                        (connected p12 p11) (connected p12 p13) (connected p12 p22)
                        (connected p13 p12) (connected p13 p23) 

    (connected p21 p11) (connected p21 p22)                     (connected p21 p31)
    (connected p22 p12) (connected p22 p21) (connected p22 p23) (connected p22 p32)
    (connected p23 p13) (connected p23 p22)                     (connected p23 p33)

    (connected p31 p21) (connected p31 p32)
    (connected p32 p22) (connected p32 p31) (connected p32 p33)
    (connected p33 p23) (connected p33 p32) 
    
    
    ; initial state
    (at s7 p11) (at s2 p12) (at s4 p13)
    (at s5 p21) (empty p22) (at s6 p23)
    (at s8 p31) (at s3 p32) (at s1 p33)
)

(:goal (and
    (at s1 p11) (at s2 p12) (at s3 p13)
    (at s4 p21) (at s5 p22) (at s6 p23)
    (at s7 p31) (at s8 p32) (empty p33)
))

)
